# World Cup API - Ansible Playbook

### Setup

First check your server, you need to have ssh access and we recommend to configure your ssh keys in `authorized_keys`.

Change the `app_host` variable in `group_vars/all` file to `<ip or domain>` and execute the following command:

> ansible-playbook -i hosts site.yml

When the instalation has been complete, your World Cup API will be available in the:

`http://<ip or domain>/worldcupapi`

### TEST in Docker container

Change the `app_host` variable in `group_vars/all` file to `127.0.0.1` and execute the following commands:

```
sudo docker run -d -P -p 80:80 -p 3000:3000 -p 22:22 --name worldcupapi-server rastasheep/ubuntu-sshd:14.04

docker cp ~/.ssh/id_rsa.pub worldcupapi-server:/root/.ssh/authorized_keys

docker exec worldcupapi-server chown root:root /root/.ssh/authorized_keys

ansible-playbook -i hosts site.yml
```